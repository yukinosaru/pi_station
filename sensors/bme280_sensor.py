# Sensor imports
import bme280
import smbus2

# Variable for sensor
print('Setting up sensor... '),
port = 1
address = 0x76
bus = smbus2.SMBus(port)
calibration_params = bme280.load_calibration_params(bus,address)
print('Done')

def fetchData():
	print('Fetching sensor data... '),
	bme280_data = bme280.sample(bus,address)
	timestamp = bme280_data.timestamp
	humidity = round(bme280_data.humidity,1)
	pressure = round(bme280_data.pressure,1)
	ambient_temperature = round(bme280_data.temperature,1)
	data = [timestamp.year,timestamp.month,timestamp.day,timestamp.hour,timestamp.minute,timestamp.second,humidity,pressure,ambient_temperature]
	print('Done')
	return data

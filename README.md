# PiStation
Utility for reading and logging readings from a low cost weather station.
Logs to a Google Sheets spreadsheet using Google Sheet API and service account (server to server)

## Sensors
1. BME280 - Air temperature, pressure, humidity
2. DS18B20 - Ground temperature

Running on a Raspberry Pi Zero W
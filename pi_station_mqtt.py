#!/usr/bin/env python

print('*******************************')
print('***       PI  STATION       ***')
print('*******************************')

# MQTT
import paho.mqtt.client as mqtt

# Outputs modules
import outputs.gsheets as gsheets

from time import sleep

# Globals for holding sensor values
values = [0,0,0,0,0,0,0,0,0,0]

# MQTT callback for CONNACK
def on_connect(client, userdata, flag, rc):
	print("Connected with result code "+str(rc))
	client.subscribe("weather/#")

# Dictionary for array indices
def p(x):
	return {
		'weather/humidity': 6,
		'weather/pressure': 7,
		'weather/temperature': 8,
		'weather/groundtemp': 9
	}[x]

# MQTT callback for message received
def on_message(client, userdata, msg):
	i = p(msg.topic)
	values[i] = str(msg.payload.decode("utf-8"))
	print(values)
#	print(msg.topic+" "+str(msg.payload.decode("utf-8")))
#	timestamp = datetime.utcnow()

#	values = [timestamp.year,timestamp.month,timestamp.day,timestamp.hour,timestamp.minute,timestamp.second,humidity,pressure,air_temp,ground_temp]
#	gsheets.logValues(values)


print("Starting monitoring. Cancel with Control-C")
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
# client.message_callback_add ("weather/pressure",log_pressure)
client.username_pw_set("username",password="password")
client.connect("192.168.1.42", 1883, 60)

client.loop_forever()

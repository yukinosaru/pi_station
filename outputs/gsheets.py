print('Connecting to Google... '),

# Google API auth imports
from googleapiclient.discovery import build
from google.oauth2 import service_account

# Globals for Google API
SCOPES = ['https://www.googleapis.com/auth/spreadsheets']
SERVICE_ACCOUNT_FILE = 'service.json'

# The ID and range of spreadsheet.
SPREADSHEET_ID = '15afyP90-ytmO-dgiU_F_cQLHIvQubG9AdCkYyTq3URs'
RANGE_NAME = 'sensor_data'

# Build Google Auth service - using service account (server to server)
credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE,scopes=SCOPES)
service = build('sheets','v4', credentials=credentials)
print('Done')

def logValues(values):
	# Format values for GSheets
	body = {
		"range": RANGE_NAME,
  		"values":[values]
	}

	# Append values to spreadsheet
	print('Writing to spreadsheet... '),
	result = service.spreadsheets().values().append(spreadsheetId=SPREADSHEET_ID,range=RANGE_NAME,valueInputOption='USER_ENTERED', body=body).execute()
	print('{0} values recorded.'.format(result.get('updates').get('updatedCells')))

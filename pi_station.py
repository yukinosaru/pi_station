#!/usr/bin/env python

print('*******************************')
print('***       PI  STATION       ***')
print('*******************************')
# Sensor modules
import sensors.bme280_sensor as bme280
import sensors.ds18b20_sensor as ds18b20

# Outputs modules
import outputs.gsheets as gsheets

from time import sleep

# Globals for customisation
POLLING_FREQ = 5 * 60

def fetchSensorData():
	values = bme280.fetchData()
	ground_temp = ds18b20.read_temp()
	values.append(ground_temp[0])
	return values

print('Starting monitoring. Cancel with Control-C')

try:
	while True:
		values = fetchSensorData()
		gsheets.logValues(values)
		sleep(POLLING_FREQ)
except KeyboardInterrupt:
	print('Monitoring stopped')

